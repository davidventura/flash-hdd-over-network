#!/bin/sh

eth_ko=$(find /lib -path "*$(uname -r)*" -name e1000.ko)
xfs_ko=$(find /lib/modules/$(uname -r) -name xfs.ko)
crc_ko=$(find /lib/modules/$(uname -r) -name libcrc32c.ko)
mkdir -p initramfs/$(dirname $eth_ko) initramfs/$(dirname $xfs_ko) initramfs/$(dirname $crc_ko)
cp $eth_ko initramfs/$(dirname $eth_ko)
cp $xfs_ko initramfs/$(dirname $xfs_ko)
cp $crc_ko initramfs/$(dirname $crc_ko)

[ ! -f test-disk.img ] && dd if=/dev/zero of=test-disk.img bs=1M count=2000
( cd initramfs/; find . | cpio -H newc -o  | gzip  > ../initramfs.igz )
