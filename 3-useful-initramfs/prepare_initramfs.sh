#!/bin/sh

eth_ko=$(find /lib -path "*$(uname -r)*" -name e1000.ko)
mkdir -p initramfs/$(dirname $eth_ko)
cp $eth_ko initramfs/$(dirname $eth_ko)

( cd initramfs/; find . | cpio -H newc -o  | gzip  > ../initramfs.igz )

