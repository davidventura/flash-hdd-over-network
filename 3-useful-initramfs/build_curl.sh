#!/bin/bash
renice -n 19 $$
cd curl
[ ! -e configure ] && ./buildconf
LDFLAGS="-ldl -lpthread -static" ./configure --enable-ares --disable-shared --disable-rt --disable-esni --disable-ftp --disable-file --disable-ldap --disable-ldaps --disable-rtsp --disable-dict --disable-telnet --disable-tftp --disable-pop3 --disable-imap --disable-smb --disable-smtp --disable-gopher --disable-manual --disable-libcurl-option --disable-ipv6 --disable-sspi --disable-ntlm-wb --disable-tls-srp --disable-unix-sockets --disable-cookies --disable-mime --disable-dateparse --disable-netrc --disable-alt-svc --with-gnutls
make curl_LDFLAGS=-all-static  -j$(nproc)
