#!/bin/sh

eth_ko=$(find /lib -path "*$(uname -r)*" -name e1000.ko)
mkdir -p initramfs/$(dirname $eth_ko)
cp $eth_ko initramfs/$(dirname $eth_ko)

[ ! -f test-disk.img ] && dd if=/dev/zero of=test-disk.img bs=1M count=2000
( cd initramfs/; find . | cpio -H newc -o  | gzip  > ../initramfs.igz )
