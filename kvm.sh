#!/bin/bash

display=gtk
base_cmd="sudo kvm -display $display -m 2048 -cpu host"
DRIVE=test-disk.img

if [ $# -eq 0 ]; then
    echo You must provide at least one of the wanted features: initramfs, network, disk
    exit 1
fi

while (( "$#" )); do
    case $1 in
        initramfs)
            base_cmd="$base_cmd -kernel /boot/vmlinuz-$(uname -r)  -initrd initramfs.igz -append 'quiet'"
            ;;
        network)
            base_cmd="$base_cmd -device e1000,netdev=net0,mac=DE:AD:BE:EF:88:39 -netdev tap,id=net0"
            ;;
        disk)
            base_cmd="$base_cmd -drive file=$DRIVE,format=raw"
            ;;
        *)
            echo $1 is not supported! exiting..
            exit 1
    esac
    shift
done

$base_cmd
