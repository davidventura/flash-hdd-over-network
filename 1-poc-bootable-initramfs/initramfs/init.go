package main
import "fmt"
import "time"

func main() {
    fmt.Println("Hi. I will sleep forever now")
    time.Sleep(2 * time.Hour)
}
