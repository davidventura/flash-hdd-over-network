#!/bin/sh
( cd initramfs/; go build init.go; find . | cpio -H newc -o > ../initramfs.raw )
kvm -kernel /boot/vmlinuz-$(uname -r) -initrd initramfs.raw
