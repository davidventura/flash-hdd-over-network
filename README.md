# Flash a disk over network, from an initramfs

This is a step-by-step set of examples and utilities used to flash a disk iamge from an initramfs, described [here](https://blog.davidventura.com.ar/flashing-linux-disk-images-from-an-initramfs.html).
