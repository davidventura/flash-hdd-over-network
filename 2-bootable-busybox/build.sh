#!/bin/sh
cd busybox
make defconfig
make clean && make LDFLAGS=-static -j$(nproc)
